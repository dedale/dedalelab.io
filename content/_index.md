+++
description = "Dedale 0.3"
myDate="March 25, 2021"
date=2021-03-25
+++

**Create your team of AI agents.**

**Make them collectively explore, hunt, collect treasures and survive.**

**__Too easy ? We'll see..__**

 - Student or enthusiastic amateur ? You will discover some of the nice (and sometimes difficult) characteristics of Multi-Agents Systems implementation. 
 - Expert ? We want you to help us advance the state of the art and compare our solutions.

In both cases, have fun with <b>dozens</b> of agents running wild !



<!--
{::comment}
This website is powered by [GitLab Pages](https://about.gitlab.com/features/pages/)
/ [Hugo](https://gohugo.io) and can be built in under 1 minute.
Literally. It uses the `beautifulhugo` theme which supports content on your front page.
Edit `/content/_index.md` to change what appears here. Delete `/content/_index.md`
if you don't want any content here.

Head over to the [GitLab project](https://gitlab.com/pages/hugo) to get started.
{:/comment}
!-->
