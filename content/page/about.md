---
title: About Dedale
<!--subtitle: Why you'd want to hang out with me!-->
comments: false
---

Dedale is initiated and maintained by members of the MAS research group from the LIP6 computer science lab.
We are studying *coordination, learning and decision-making processes* to improve the autonomy of artificial entities within *unknown, dynamic, partially observable and asynchronous environments*.

Dedale aims to facilitate and improve the experimental evaluation conditions
of the developed MAS algorithms, and to contribute to the progress of the field towards
decentralised solutions able to deal with real-world situations.

One of Dedale's goal is thus to familiarize people to adress multi-agent problems that go beyond team-of-2 agents and fully-observable, synchronized and static setups. The variety of modelable multi-agents problems associated with the possibility to create a peer-to-peer network of Dedale's environments makes us believe Dedale beeing able to become a unifying environment for both MAS research and teaching communities in their goal to work under real-life hypotheses.

Its peer-to-peer feature also opens new perspectives with the possibility for researchers to study agents' capabilities to remain autonomous on a long term basis at minimal cost. Finally, if easily reproducible experiments with teams of dozens of agents are not common today, we believe that Dedale provides a step towards this goal.

In Dedale's educational dimension, the platform is used within the [master Androide](http://androide.lip6.fr/) of [Sorbonne University](http://www.sorbonne-universites.fr/), Paris, France, since 2016. In the course [FoSyMa](http://androide.lip6.fr/?q=node/21) (from the French "Fondement des Systèmes Multi-Agents"), it allows students to discover some of the difficulties that comes with agents and distributed systems : 

 * autonomy, decision and coordination under uncertainty,
 * distribution and asynchronism (system and communication)


---

As this project is intended for Master students an above, we assume a minimum level in Computer Science :

 1. You know Java or at least an object oriented language,
 2. You know what is a thread, and are familiar with the notion of Deep-First Search/Best-first Search, 
 3. You are familiar with the [Jade Multi-Agent framework](https://jade-project.gitlab.io/) on which we stand on. If that is not the case, we invite you to take a few hours (~4) to discover it using this [presentation](https://jade-project.gitlab.io/docs/Jade-multiagent-platform_Principles-and-main-functionalities_Herpson-2023.pdf) and the [start-jade](http://startjade.gitlab.io) project's tutorials.

---

Dedale is developped in Java and packaged for Eclipse. It stands on various libraries, both from the MAS research group that develops the project and the community. Regarding the latter, we are currently using :

 * [JADE](https://jade-project.gitlab.io/) : A Multi-Agents platform
 * [GraphStream](http://graphstream-project.org/) : A dynamic graph library
 * [JME3](http://jmonkeyengine.org/) : A 3D game engine (integration in progress) 
 * [Weka](http://www.cs.waikato.ac.nz/ml/weka/) : A machine learning library
 * [JavaFx](https://openjfx.io/): The new Java Gui
 * [Junit](https://junit.org/junit5/) : Testing framework
 * [Maven](https://maven.apache.org/) : Software for building and managing Java-based project
 

