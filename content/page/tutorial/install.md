# How to install and Execute Dedale ?

Dedale is developped in Java and packaged for Eclipse (but fully functional on any maven-compatible IDE)

### Install Java and Eclipse

1. [JDK 21 or more recent](https://jdk.java.net/23/)
2. [Eclipse IDE for Java developpers](http://www.eclipse.org/downloads/packages/)
3. Make sure that the default JDK called in your IDE is JDK21 or more recent

### Download the project

Using Eclipse:

 1. _File_>_Import_>_Git_>_Projects from Git_> _Clone URI_
 2. Paste in the URI field : [https://gitlab.com/dedale/dedale-etu.git ](https://gitlab.com/dedale/dedale-etu.git) 
 3. _Next_>_Next (select the master branch)_>_Next_>_Next_>_Finish_
 4. Eclipse automatically build the project, wait for it to finish (bottom right).

The alternative is for you to download it manually then to import the project in the IDE :

* Using git manually : git clone https://gitlab.com/dedale/dedale-etu.git 
* Downloading the archive from this page (https://gitlab.com/dedale/dedale-etu)


### Launch the program within Eclipse:

1. Right-Click on your project and select _Maven/Maven install_ (It will download all the dependencies/javadocs/)
2. Select the Principal.java file, Right-Click and select _run as../Java application_.

Enjoy :)

Here will appear 2 Youtube videos that will detail the installation steps.
