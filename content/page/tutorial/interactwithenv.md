# How can an agent interact with the environment ?

1. Your agent needs to extend the AbstractDedaleAgent class and add its initial behaviours to startBehaviours()

2. Your agent must use the Dedale's API to interact with the environment


## 1. Simple agent structure

```java
public class DummyMovingAgent extends AbstractDedaleAgent{

	private static final long serialVersionUID = -2991562876411096907L;
	

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){
		super.setup();

		//get the parameters given when creating the agent into the object[]
		final Object[] args = getArguments();
		//use them as parameters for your behaviours 
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		/************************************************
		 * 
		 * ADD the behaviours of you agent here
		 * 
		 ************************************************/
		lb.add(new RandomWalkBehaviour(this));
		lb.add(new SayHello(this));
		
		
		/***
		 * MANDATORY TO ALLOW YOUR AGENT(S) TO BE DEPLOYED CORRECTLY WITH DEDALE
		 */
		
		addBehaviour(new startMyBehaviours(this,lb));

	}


	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){
		super.takeDown();
	}
	
	/**
	 * This method is automatically called before migration. 
	 * You can add here all the saving you need
	 */
	protected void beforeMove(){
		super.beforeMove();
	}
	
	/**
	 * This method is automatically called after migration to reload. 
	 * You can add here all the info regarding the state you want your agent to restart from 
	 * 
	 */
	protected void afterMove(){
		super.afterMove();
	}
 ```

## 2. Environment's API

The complete API is available [here](https://dedale.gitlab.io/API/eu/su/mas/dedale/mas/AbstractDedaleAgent.html)

The key methods that you will use are :

* Observation, communication and deplacement :
	* *String getCurrentPosition()* : Returns the current position of the agent
	* *List&lt;Couple&lt;String,List&lt;Couple&lt;Observation,Integer&gt;&gt;&gt;&gt; observe()* : Returns the set of observables that can be perceived from the agent current position as a list of couple (position,list(ObservationType,Value)
	* *boolean moveTo(String myDestination)* : Makes your agent move to myDestination (if reachable). This functino must be the last one called within your behaviour
	* *void sendMessage(ACLMessage msg)* : Send a message and manage the communication radius. You must use only this method when communicating.
		* *msg.setContent(String s)* : To set a String as the message content
		* *msg.setContentObject(Serializable o)* : To send a serialzable object. Note that you **mustn't** use both methods at the same time.

* Treasures and safes
	* *String getMyTreasureType()*:  Type of treasure that the agent can grab (only one type per agent)
	* *Set &lt;Couple&lt;Observation,Integer&gt; getMyExpertise()* : Expertise of the agent
	* *int getBackPackFreeSpace()*: Space available in the agent backpack to store treasures
	* *boolean openLock(Observation o)*: Open the safe (type Gold or Diamond) if the required expertise is provided. This method agregate all the expertises of the agents connex to the agen triggering this method.
	* *int pick()*: Grab all of any of the treasure available on the current position (according to agent type, capacity and safe state)
	* *boolean EmptyMyBackPack(String agentSiloName)* : Allow the agent to transfer its backpack within the Silo (Tanker) agent if it is in the vicinity

* Fights
	* *void throwGrenade(String locationId)* : Attack all agents present on the given location (if any and if the location is reachable)

See the DummyMovingAgent and its RandomWalkBehaviour for an operational example of the API.

Here is a video tutoral illustrating the use of the API

Once your agent is written you just have to [deploy it](https://dedale.gitlab.io/page/tutorial/deployagents).

