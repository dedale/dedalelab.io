# How to configure the agents

{{< figure link="/img/dedaleConfigAgents.png" title="Dedale configuration process" width="50">}}

The 4 steps of the platform configuration can be modified independently. 
We present here the 2 steps related to the agents configuration : Defining the agents type & characteristics and implementing them.
We assume that the two first steps related to the environment configuration are done. If not, [configure your environment here.](https://dedale.gitlab.io/page/tutorial/configureenv/)
We will now see how to :

* Configure your agents characteristics
* Create java agents able to interact with the environment

# 1. Defining entities types and characteristics
Entities characteristics refers to the capabilities of your agents within the environment. They are defined in the *entities.json* files in the *resources* folder. This file is given to the system when you configure the environment. The structure of the file is the following :

 ```
[
 {
  "agentType":"X",
  "agentName": "X",
  "communicationRange": Y,
  "initialLocation": "X",
  "backPackCapacityGold": Y,
  "backPackCapacityDiamond":Y, 
  "detectionRadius": Y,
  "strengthExpertise": Y,
  "lockPickingExpertise":Y
 },
 {
  "agentType":"X",
  "agentName": "X",
  "communicationRange": Y,
  "initialLocation": "X",
  "backPackCapacityGold": Y,
  "backPackCapacityDiamond":Y, 
  "detectionRadius": Y,
  "strengthExpertise": Y,
  "lockPickingExpertise":Y
 }
]
 ```

with :

- *agentType* : wumpus, agentExplo, agentCollect, agentTanker,HumanControlled. The platform adapts the actions available to one agent within the environment according to its type. Wumpus are bots. Agents Explo and tanker are types of agents that cannot pick resources, only collector agents can. The size of one's backPack is not taken into account. Only one agent of type HumanControlled is possible. This last agent is keyboard controlled (**N**(ext) and **O**(k)) and can only be used on your local computer.
- *communicationRange* : maximal radius authorised for this agent to communicate with others
- *initialLocation* : nodeId to deploy the agent or *free* if the system can choose randomly
- *BackPackCapacityGold* : maximum quantity of resources that the agent can transport. -1 if none.
- *BackPackCapacityDiamond* : maximum quantity of resources that the agent can transport. -1 if none.
- *detectionRadius* : distance from which the agent can be perceived
- *lockpicking* : level of lockpicking expertise of the agent [0,maxint]
- *strength* : level of strength the agent [0,maxint]


Illustrative example of an entities configuration file (available in the *resources* folder).
 ```
[
{ 
  "agentType":"agentCollect",
  "agentName": "Collect",
  "communicationRange": 3,
  "initialLocation": "119",
  "backPackCapacityGold": 20,
  "backPackCapacityDiamond":30, 
  "detectionRadius": 0,
  "strengthExpertise": 1,
  "lockPickingExpertise":1
  },
{ 
  "agentType":"agentTanker",
  "agentName": "Tank",
  "communicationRange": 3,
  "initialLocation": "free",
  "backPackCapacityGold": 100,
  "backPackCapacityDiamond":100, 
  "detectionRadius": 0,
  "strengthExpertise": 1,
  "lockPickingExpertise":1
  }
]
 ```
 
# 2. Create and deploy your Jade agents 

## 2.1 Create an agent that can interact with the environment

* Your agent needs to extend the AbstractDedaleAgent class and add its initial behaviours to startBehaviours()

* Your agent must use the Dedale's API to interact with the environment

### Simple agent structure

```java
public class DummyMovingAgent extends AbstractDedaleAgent{

	private static final long serialVersionUID = -2991562876411096907L;
	

	/**
	 * This method is automatically called when "agent".start() is executed.
	 * Consider that Agent is launched for the first time. 
	 * 			1) set the agent attributes 
	 *	 		2) add the behaviours
	 *          
	 */
	protected void setup(){
		super.setup();

		//get the parameters given when creating the agent into the object[]
		final Object[] args = getArguments();
		//use them as parameters for your behaviours 
		
		List<Behaviour> lb=new ArrayList<Behaviour>();
		
		/************************************************
		 * 
		 * ADD the behaviours of you agent here
		 * 
		 ************************************************/
		lb.add(new RandomWalkBehaviour(this));
		lb.add(new SayHello(this));
		
		
		/***
		 * MANDATORY TO ALLOW YOUR AGENT(S) TO BE DEPLOYED CORRECTLY WITH DEDALE
		 */
		
		addBehaviour(new startMyBehaviours(this,lb));

	}


	/**
	 * This method is automatically called after doDelete()
	 */
	protected void takeDown(){
		super.takeDown();
	}
	
	/**
	 * This method is automatically called before migration. 
	 * You can add here all the saving you need
	 */
	protected void beforeMove(){
		super.beforeMove();
	}
	
	/**
	 * This method is automatically called after migration to reload. 
	 * You can add here all the info regarding the state you want your agent to restart from 
	 * 
	 */
	protected void afterMove(){
		super.afterMove();
	}
 ```

### Environment's API

The complete API is available [here](https://dedale.gitlab.io/API/eu/su/mas/dedale/mas/AbstractDedaleAgent.html)

The key methods that you will use are :

* Observation, communication and movement :
	* *Location getCurrentPosition()* : Returns the current position of the agent
	* *List&lt;Couple&lt;Location,List&lt;Couple&lt;Observation,String&gt;&gt;&gt;&gt; observe()* : Returns the set of observables that can be perceived from the agent current position as a list of couple (position,list(ObservationType,Value)
	* *boolean moveTo(Location myDestination)* : Makes your agent move to myDestination (if reachable). This function must be the last one called within your behaviour
	* *void sendMessage(ACLMessage msg)* : Send a message and manage the communication radius. You must use only this method when communicating.
		* *msg.setContent(String s)* : To set a String as the message content
		* *msg.setContentObject(Serializable o)* : To send a serializable object. Note that you **mustn't** use both methods at the same time.

* Treasures and safes
	* *Observation getMyTreasureType()*:  Type of treasure that the agent can grab (only one type per agent)
	* *Set &lt;Couple&lt;Observation,Integer&gt; getMyExpertise()* : Expertise of the agent
	* *int getBackPackFreeSpace()*: Space available in the agent backpack to store treasures
	* *boolean openLock(Observation o)*: Open the safe (type Gold or Diamond) if the required expertise is provided. This method aggregate all the expertises of the agents connex to the agent triggering this method.
	* *boolean closeLock()*: Close all the safes present at the current location
	* *int pick()*: Grab all of any of the treasure available on the current position (according to agent type, capacity and safe state)
	* *void dropOff()* : Drop the treasure contained in the agent's backpack on the current position, if allowed (depending of the agent's type) and regarding elements already on the current location (one type of treasure per node)
	* *boolean EmptyMyBackPack(String agentSiloName)* : Allow the agent to transfer its backpack within the Silo (Tanker) agent if it is in the vicinity

* Fights
	* *void throwGrenade(Location locationId)* : Attack all agents present on the given location (if any and if the location is reachable)

See the DummyMovingAgent and its RandomWalkBehaviour for an operational example of the API.

Here is a video tutorial illustrating the use of the API

Once your agent is written you just have to deploy it.


## 2.2 Deploy your agents 

In the *Principal.java* file, you just have to add your agents in the *createAgent()* method, under the *ADD YOUR AGENTS HERE* comment :

 ```java
 
//1) Get the container where the agent will appear
c = containerList.get(ConfigurationFile.LOCAL_CONTAINER2_NAME);
			
//2) Give the name of your agent, MUST be the same as the one given in the entities file.
agentName="Explo1";
	
//3) If you want to give specific parameters to your agent, add them here
Object [] entityParameters2={"My parameters"};

//4) Give the class name of your agent to let the system instantiate it
ag=createNewDedaleAgent(c, agentName, DummyMovingAgent.class.getName(), entityParameters2);
	
agentList.add(ag);
```

That's it, you can Select the Principal.java file, Right-Click on it and select run as../Java application.


Here is a video tutorial illustrating agent's deployment.

