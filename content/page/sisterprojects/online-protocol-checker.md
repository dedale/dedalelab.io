# Online protocol checker

When creating a new protocol, several differencies might initialy appear between the paper-version of the protocol and the implemented one. 
To help users get a working protocol, we developed a ***monitor agent*** whose goal is to check on-the-fly if there is a difference between the expected behaviour and the occuring one.

This sister-project, which will work on any JADE project, requires the users to : 
1. Describe their protocol in AUML, which can be very easily done in [PlantUml](https://plantuml.com/fr/sequence-diagram) with the following [online editor](http://www.plantuml.com/plantuml/uml/SoWkIImgAStDuNBCoKnELT2rKt3AJx9IS2mjoKZDAybCJYp9pCzJ24ejB4qjBk42oYde0jM05MDHLLoGdrUSoeLkM5u-K5sHGY9sGtqJIuj0ocrqTI7g4s7gSHZla9gN0dGo0000)
2. Add the resulting AUML in a comment section in their java class,
2. Deploy the monitor on their platform,
3. Give the AUML to the monitor.

Currently under development, an alpha-release will be soon deployed. Once stable, the monitor will be shipped by default with Dedale.