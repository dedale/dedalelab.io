# OSM import to Dedale (gs-geography)

This sister-project allow users to import and format OpenStreetMap data so that it can be used as environment within Dedale (and any other project based on graphStream).

* [The repo is here](https://gitlab.com/dedale/gs-geography)
* For any question related to OSM import, join us on [Dedale's discord](https://discord.gg/rx7fWWf) in the Tools/s-geography channel

We stand on the now discontinued [gs-geography project](https://github.com/graphstream/gs-geography) developped by the GraphStream developers. Thanks a lot for their work. As their was no response from them, we forked it and updated it so that it could work with current libraries and added some simple functionnalities.

 * It now supports gs 2.0-alpha, gs ui-swing-2.0-alpha, geotool 23 and java > 8,
 * We added the conversion from pbf 2 osm so that we can use more data sources,
 * We added simple API calls.

This project require a lot of big dependencies in order to be able to run. That's why we did not integrate it in Dedale natively.


0. Download the project and install it as a maven project. Maven build (or install) will take a while to get all dependencies. All the duration indicated bellow start after this step is done.

## How to quickly & easily use a very small OSM area within Dedale ? (5 mins)

1. Go to OpenStreetMap and [choose your area](https://www.openstreetmap.org/search?query=ellis%20island#map=17/40.69869/-74.04070)


{{< figure link="/img/sister-projects/osm/osm-Ellis-Island.png" title="Extracting a small OSM file from openStreetMap" width="85">}}

2. Select "Export"
3. Select, "manually select the area" to further optimise the extracted location
 * If the website either says "This area is too large to be exported in OpenStreetMap XM format." and do not offer a download button or allow you to download but then says "You requested too many nodes (limit is 50000). Either request a smaller area, or use planet.osm", you loose. Change the area size or drop this quick & easy solution for the complex one (see below)
 * If the download is successfull, congrats, the longest part is done.
4. In the gs-geography project, use the ___Test_OSM_RoadNetwork2.java___ file or create your own java file with:

```
public static void main(String args[]) {

	List<String> ls= new ArrayList<String>();
	ls.add("highway");

	GeoSourceOSM_RoadNetwork.generateGsFromOSM(ls, System.getProperty("user.dir")+"/data/YourMAP.osm");

}
```
5) That's it. You can now copy the resulting dgs file and enjoy this real topology in Dedale.

## How to get an intermediary sized file from OSM to Dedale ? (6 mins)

1. Go to [overpass turbo](http://overpass-turbo.eu/#)
2. Select the area you are looking and call the "assistant" with : highway=* and type:way
3. Select export then download as "GeoJSON"

{{< figure link="/img/sister-projects/osm/overPass.png" title="Extracting a medium GeoJSON file from openStreetMap" width="85">}}


4. Download and launch a [desktop editor](https://wiki.openstreetmap.org/wiki/Editors). We recommend [JOSM](https://josm.openstreetmap.de/)
5. Open the previously downloaded file and remove unnecessary data
6. Export as an osm file
7. Call the ___generatGsFromOSM___ method presented above on this file and its done.
8. That's it. You can now copy the resulting dgs file and enjoy this real topology in Dedale.

## How to get a big topology from OSM to Dedale ? (several hours long and unoptimized process)

1. You will need to download a pbf file (compressed version of an OSM). 
 * 1.a) If its a file of reasonnable size, you can download it from OSM websiste, step 1 above, by clicking on "API-overpass".
 * 1.b) If still too big, go to [Planet OSM](https://planet.openstreetmap.org/) and download the area you need.
2. Install and use one of the available [pbf editor](https://wiki.openstreetmap.org/wiki/Editors) (JSOM is one of them).
3. Extract the sub-area you are looking for and save it into a pbf file. Reduce the area as much as possible and remove unneeded informations to diminish the weight of the file.
4. Look for the  ___Test_pbf2OSM.java___ file and change the name of your file or create your own java file with : 
```
public static void main(String args[]) {

	List<String> ls= new ArrayList<String>();
	ls.add("highway");

	GeoSourceOSM_RoadNetwork.generateGsFromPBF(ls, System.getProperty("user.dir")+"/data/YourMAP.pbf");
}
```
5. Check that you have several free Gig on your hard-drive and RAM, run the program and go to sleep.

The process is definitely not optimized as the system will do PBF --> OSM --> DGS with an at least x100 factor in size between the pbf and the osm. But at least its working. Switch the osm and directly work with the pbf could be considered in the future is this feature becomes highly used.