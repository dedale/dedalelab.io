
+++
title = "2021a Release"
description = "Release"
date=2021-03-21
+++
 COVID delayed all work of 1 year. We had to reschedule everything from the 2020roadmap as new features were needed.
 What is done :
 - Removes the ingraph global statistics and replace them with individual stats regarding the backpacks (observer pattern)
 - Add choice between GS and JME environments. This change the parameters to be given to the agents at creation. Non-backward compatibility 
 - GS : 
   - A default behaviour for cooperative exploration is offered (non-optimised)
   - A new generator (Barabasi) is added for tree generation
   - A human controlled agent for displacements is offered : N(ext) and O(k)
   - An agent can now posses up to 2 type of treasures in its backpack simultaneously (and pick accordingly). DropOff remains unchanged
   - Only one type of treasure can be dropped on a given node (wumpus side)
   - Small change in the environment API (designer side)
   - Supports GS2.0 stable (instead of 2-alpha). Change the way to manipulate node and edges + JavaFx full compatibility
   - GUI 
     - Offers stench for all types of agents (will in the future be changed to noise)

All these update are available on both Dedale and on the end-user instance (Dedale-etu repo), which mask lots of details to the end-user.

 
{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/dedale-v024-Gui.png" caption="dedale v0.24 - GUI - Com and ressources ">}}
{{< /gallery >}}

What remain to be released on the public repo (available for research partners):
 - (be) Integrating agents automatic log
 - (be) Simplify each agent communication range configuration
 - (be) Adding a capacity to nodes in order to allow them to have several agents on a given position (and thus simplifiying the openLock computation).
 - (befe) Adding API for distant-agent control
 - (fe) Integrating JME 3 for treasure hunt
