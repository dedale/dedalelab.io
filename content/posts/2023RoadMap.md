
+++
title = "March 2023 intermediary release"
description = "2023 RoadMap"
date=2023-02-06
+++

 - Code

   - Improvement of the API genericity
   - Adding a kill-switch for each agent accessible through the GUI 
   - Adding a method to the API to allow some agents to close safe and thus add one layer of dynamicity in the environment
   - Minor GUI improvments
   - JavaFX 18 support
   - Weka (machine learning) integration

 - Documentation
   - Update website and examples