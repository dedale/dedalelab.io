+++
title = "2019 Roadmap"
description = "Dedale 0.2 "
date=2019-07-10
+++

* Communication: 
	 - Updating the website
	 - Discord channel for Dedale's support tchat
	 - Draft version of Dedale's research paper

* Features:
	 - (be) Moving from graphStream 1.2 to graphStream 2.0
	 - (fe) Offering a new GUI for the Gatekeeper with JavaFX
	 - (fe) Allowing OSM-based maps

	 - ((befe) Adding a continious and 3D environment for Hunting (JME3))

* **Release delivery : December 2019**

{{< gallery caption-effect="fade" >}}
  {{< figure link="/img/dedale-v0.21-alpha.png" caption="dedale v0.21 - GUI OSM - alpha ">}}
  {{< figure link="/img/dedale-v0.22-duel-alpha.png" caption="dedale v0.22 - GUI 3D env - alpha ">}}
{{< /gallery >}}

