
+++
title = "News regarding the JME integration"
description = "RoadMap"
date=2021-03-22
+++
 - The COVID delayed JME integration of 1 year.
 - The last stable JME release (april 2020) significantly changes the way to interact with the engine.
 - The alpha-version currently used for the hunt case as a test outside of Dedale and proposed by external devs is not satisfactory
   -  high computational costs for observe(), and imperfect results
   -  starting conditions for agents non-equalitarian
   - Random map generator inconsistent
   - Non easily automated repeated experiments
 
A complete reimplementation is underway by Dedale's team.
The JME integration is thus postponed.
