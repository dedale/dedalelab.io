
+++
title = "May 2024 release"
description = "2024 RoadMap"
date=2024-05-22
+++

 - Code

   - [Feature] Adding neighbours' agents visibility
   - [Feature] Adding Json support for map's elements description
   - [Backend] Maven genericity 
   - [Backend] JavaFX and JDK 21 support

 - Documentation
   - Update website and examples