
+++
title = "2020 Roadmap"
description = "RoadMap"
date=2020-01-05
+++

 - (be) Bug fixes
 - (befe) Discourse instance for Dedale's forum
 - (fe) Update website documentation and tutorials

 - (fe) Integrating the current state of the agents' backPack directly within the GUI
 - (fe) Integrating communication counter 
 - (be) Integrating agents automatic log
 - (be) Simplify each agent communication range configuration

 - (be) Adding a capacity to nodes in order to allow them to have several agents on a given position (and thus simplifiying the openLock computation).
 - (befe) Adding API for distant-agent control 

 - (fe) Integrating JME 3 for treasure hunt

 - **Stable realease : July 2020**
