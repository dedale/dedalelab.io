
+++
title = "Adding safe"
date=2019-02-05
description = "Safes added to the public environments."
+++

From now on, treasures can be stored within safes on the map. 

 - Agents will need to use their strength and lockpicking expertise to open them before grabbing gold and diamond.

 - Several agents can team up to open a safe. They just need to be linked to the one on the safe location by forming a path.
